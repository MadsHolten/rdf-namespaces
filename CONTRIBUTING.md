If you want to add a namespace to this project, edit `src/generate.ts`.
Add the namespace you want to add, keyed by the alias you want to give it - it's pretty
straightforward.

Then submit a merge request. We'll then generate a test version that includes the generated
namespace files, and if that works, we can release a new version.
