rdf-namespaces
======
Automatically generated TypeScript files containing short aliases to common RDF namespaces.
This allows you to use `rdf.type` instead of `'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'` in
your code, and gives you code completion for available properties if you use an editor with
TypeScript support.

For a list of included vocabularies, see [the source code](https://gitlab.com/vincenttunru/rdf-namespaces/blob/master/src/generate.ts#L5).

## Installation

```bash
npm install rdf-namespaces
```

## Usage

```typescript
import { ldp } from 'rdf-namespaces';

const ldpResource: string = ldp.Resource; // http://www.w3.org/ns/ldp#Resource
```

## Changelog

See [CHANGELOG](https://gitlab.com/vincenttunru/rdf-namespaces/blob/master/CHANGELOG.md).

## License

MIT © [Inrupt](https://inrupt.com)
